using Android.App;
using Android.OS;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Com.H6ah4i.Android.Widget.Advrecyclerview.Swipeable;
using MvvmCross.AdvancedRecyclerView;
using MvvmCross.AdvancedRecyclerView.Adapters.NonExpandable;
using MvvmCross.AdvancedRecyclerView.Data.EventArguments;
using MvvmCross.AdvancedRecyclerView.ViewHolders;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Droid.Support.V7.RecyclerView.Model;
using RssClient.Core.ViewModels;
using RssClient.Droid.RecyclerViewExtensions;

namespace RssClient.Droid.Activities
{
    [Activity]
    public class RssFeedsActivity : BaseActivity<RssFeedsViewModel>
    {
        private MvxAdvancedNonExpandableRecyclerView _feedsRecyclerView;
        private MvxNonExpandableAdapter _mAdapter;
        protected override int LayoutResource => Resource.Layout.activity_rss_feeds;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SupportActionBar.SetDisplayHomeAsUpEnabled(false);

            _feedsRecyclerView = FindViewById<MvxAdvancedNonExpandableRecyclerView>(Resource.Id.rss_feeds_recyclerview);

            var mLayoutManager = new MvxGuardedLinearLayoutManager(this) {Orientation = LinearLayoutManager.Vertical};
            _feedsRecyclerView.SetLayoutManager(mLayoutManager);
            var dividerItemDecoration = new DividerItemDecoration(this, mLayoutManager.Orientation);
            dividerItemDecoration.SetDrawable(ContextCompat.GetDrawable(this, Resource.Drawable.recycler_view_divider));
            _feedsRecyclerView.AddItemDecoration(dividerItemDecoration);

            _mAdapter = _feedsRecyclerView.AdvancedRecyclerViewAdapter as MvxNonExpandableAdapter;
            if (_mAdapter != null)
            {
                _mAdapter.SwipeResultActionFactory = new RemoveSwipeResultActionFactory();
                _mAdapter.MvxViewHolderBound += MAdapterOnMvxViewHolderBound;
                _mAdapter.SwipeBackgroundSet += MAdapterOnSwipeBackgroundSet;
            }
        }

        private void MAdapterOnSwipeBackgroundSet(MvxSwipeBackgroundSetEventArgs args)
        {
            var bgRes = 0;
            switch (args.Type)
            {
                case SwipeableItemConstants.DrawableSwipeNeutralBackground:
                    bgRes = Resource.Drawable.bg_swipe_item_neutral;
                    break;
                case SwipeableItemConstants.DrawableSwipeLeftBackground:
                    bgRes = Resource.Drawable.bg_item_swiping_state;
                    break;
            }

            if (bgRes != 0) args.ViewHolder.ItemView.SetBackgroundResource(bgRes);
        }

        private void MAdapterOnMvxViewHolderBound(MvxViewHolderBoundEventArgs args)
        {
            if (args.Holder is MvxAdvancedRecyclerViewHolder swipeHolder)
            {
                var swipeState = swipeHolder.SwipeStateFlags;

                if ((swipeState & SwipeableItemConstants.StateFlagIsUpdated & 0) == 0)
                {
                    int backgroundResourceId;
                    if ((swipeState & SwipeableItemConstants.StateFlagIsActive) != 0)
                        backgroundResourceId = Resource.Drawable.bg_item_swiping_active_state;
                    else if ((swipeState & SwipeableItemConstants.StateFlagSwiping) != 0)
                        backgroundResourceId = Resource.Drawable.bg_item_swiping_state;
                    else
                        backgroundResourceId = Resource.Drawable.bg_item_normal_state;

                    swipeHolder.UnderSwipeableContainerView.SetBackgroundResource(backgroundResourceId);
                }

                swipeHolder.MaxLeftSwipeAmount = -0.3f;
                swipeHolder.MaxRightSwipeAmount = 0;

                swipeHolder.SwipeItemHorizontalSlideAmount =
                    _mAdapter.SwipeItemPinnedStateController.ForRightSwipe().IsPinned(args.DataContext) ? -0.3f : 0;

                var hasOnClickListeners = swipeHolder.SwipeableContainerView.HasOnClickListeners;
                if (!hasOnClickListeners)
                {
                    swipeHolder.SwipeableContainerView.LongClick += (sender, eventArgs) =>
                    {
                        ViewModel.EditFeedCommand.Execute(swipeHolder.DataContext);
                    };

                    swipeHolder.SwipeableContainerView.Click += (sender, eventArgs) =>
                    {
                        ViewModel.SelectFeedCommand.Execute(swipeHolder.DataContext);
                    };
                }
            }
        }
    }
}