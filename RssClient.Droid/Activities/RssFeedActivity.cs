﻿using Android.App;
using Android.Content;
using Android.Net;
using Android.OS;
using Android.Views;
using MvvmCross.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MvvmCross.ViewModels;
using RssClient.Core.ViewModels;

namespace RssClient.Droid.Activities
{
    [MvxActivityPresentation]
    [Activity]
    public class RssFeedActivity : BaseActivity<RssFeedViewModel>
    {
        private IMvxInteraction<string> _openWebLink;
        protected override int LayoutResource => Resource.Layout.activity_rss_feed;

        public IMvxInteraction<string> OpenWebLink
        {
            get => _openWebLink;
            set
            {
                if (_openWebLink != null)
                {
                    _openWebLink.Requested -= OnOpenWebLinkRequested;
                }

                _openWebLink = value;
                _openWebLink.Requested += OnOpenWebLinkRequested;
            }
        }

        private void OnOpenWebLinkRequested(object sender, MvxValueEventArgs<string> e)
        {
            var uri = Uri.Parse(e.Value);
            var intent = new Intent(Intent.ActionView, uri);
            StartActivity(intent);
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            var set = this.CreateBindingSet<RssFeedActivity, RssFeedViewModel>();
            set.Bind(this).For(view => view.OpenWebLink).To(viewModel => viewModel.OpenWebLinkInteraction).OneWay();
            set.Apply();

            Toolbar.Title = ViewModel.RssName;
            Title = ViewModel.RssName;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                ViewModel.CloseCommand.Execute();
            }

            return base.OnOptionsItemSelected(item);
        }

        public override void OnBackPressed()
        {
            ViewModel.CloseCommand.Execute();
        }
    }
}