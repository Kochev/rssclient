﻿using Android.App;
using Android.Widget;
using MvvmCross;
using MvvmCross.Platforms.Android;
using RssClient.Core.Services.Interfaces;

namespace RssClient.Droid.Services
{
    public class AndroidAlertService : IAlertService
    {
        public void ShowLongToast(string message)
        {
            var context = Mvx.IoCProvider.Resolve<IMvxAndroidCurrentTopActivity>()?.Activity;
            context?.RunOnUiThread(() => Toast.MakeText(Application.Context, message, ToastLength.Long).Show());
        }
    }
}