﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using MvvmCross.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MvvmCross.ViewModels;
using RssClient.Core.ViewModels;

namespace RssClient.Droid.Dialogs
{
    [MvxDialogFragmentPresentation]
    [Register(nameof(CreateOrEditRssFeedDialog))]
    public class CreateOrEditRssFeedDialog : MvxDialogFragment<CreateOrEditRssFeedViewModel>
    {
        private IMvxInteraction<string> _changeNameEditTextError;
        private IMvxInteraction<string> _changeUrlEditTextError;
        private InputMethodManager _inputMethodManager;
        private EditText _nameEditText;
        private EditText _urlEditText;
        private View _view;

        public CreateOrEditRssFeedDialog()
        {
        }

        public CreateOrEditRssFeedDialog(IntPtr intPtr, JniHandleOwnership jniHandleOwnership) : base(intPtr,
            jniHandleOwnership)
        {
        }

        public IMvxInteraction<string> ChangeNameEditTextError
        {
            get => _changeNameEditTextError;
            set
            {
                if (_changeNameEditTextError != null) _changeNameEditTextError.Requested -= ChangeEditTextErrorOnRequested;

                _changeNameEditTextError = value;
                _changeNameEditTextError.Requested += ChangeEditTextErrorOnRequested;
            }
        }

        public IMvxInteraction<string> ChangeUrlEditTextError
        {
            get => _changeUrlEditTextError;
            set
            {
                if (_changeUrlEditTextError != null) _changeUrlEditTextError.Requested -= ChangeUrlEditTextErrorOnRequested;

                _changeUrlEditTextError = value;
                _changeUrlEditTextError.Requested += ChangeUrlEditTextErrorOnRequested;
            }
        }

        public override void OnCancel(IDialogInterface dialog)
        {
            _inputMethodManager.HideSoftInputFromWindow(_nameEditText.WindowToken, 0);
            ViewModel.CloseCommand.Execute();
        }

        private void ChangeUrlEditTextErrorOnRequested(object sender, MvxValueEventArgs<string> e)
        {
            var error = e.Value;
            _urlEditText.Error = string.IsNullOrEmpty(error) ? null : error;
        }

        private void ChangeEditTextErrorOnRequested(object sender, MvxValueEventArgs<string> e)
        {
            var error = e.Value;
            _nameEditText.Error = string.IsNullOrEmpty(error) ? null : error;
        }
        
        public override Dialog OnCreateDialog(Bundle savedState)
        {
            this.EnsureBindingContextIsSet();

            var view = this.BindingInflate(Resource.Layout.dialog_create_or_edit_rss_feed, null);

            var dialog = new AlertDialog.Builder(Activity);
            dialog.SetTitle(ViewModel.Title);
            dialog.SetView(view);
            dialog.SetNegativeButton("Отмена", (s, a) => { Dialog.Cancel(); });
            dialog.SetPositiveButton("Сохранить", (s, a) =>
            {
                _inputMethodManager.HideSoftInputFromWindow(_nameEditText.WindowToken, 0);
                ViewModel.SaveRssFeedCommand.Execute();
            });

            var created = dialog.Create();
            created.Show();

            var positiveButton = created.GetButton((int) DialogButtonType.Positive);

            _inputMethodManager = (InputMethodManager) Context.GetSystemService(Context.InputMethodService);

            _nameEditText = view.FindViewById<EditText>(Resource.Id.dialog_create_or_edit_rss_name);
            _nameEditText.RequestFocus();

            _urlEditText = view.FindViewById<EditText>(Resource.Id.dialog_create_or_edit_rss_url);

            _inputMethodManager.ToggleSoftInput(ShowFlags.Forced, HideSoftInputFlags.NotAlways);

            var set = this.CreateBindingSet<CreateOrEditRssFeedDialog, CreateOrEditRssFeedViewModel>();
            set.Bind(positiveButton).For(v => v.Enabled).To(viewModel => viewModel.CanSave);
            set.Bind(this).For(v => v.ChangeNameEditTextError).To(viewModel => viewModel.ChangeNameEditError).OneWay();
            set.Bind(this).For(v => v.ChangeUrlEditTextError).To(viewModel => viewModel.ChangeUrlEditError).OneWay();
            set.Apply();


            return created;
        }
    }
}