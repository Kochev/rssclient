﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MvvmCross.ViewModels;

namespace RssClient.Droid
{
    public class TestAppPresenter : MvxAppCompatViewPresenter
    {
        public TestAppPresenter(IEnumerable<Assembly> androidViewAssemblies) : base(androidViewAssemblies)
        {
        }

        protected override Task<bool> ShowActivity(Type view, MvxActivityPresentationAttribute attribute,
            MvxViewModelRequest request)
        {
            CurrentActivity.OverridePendingTransition(Resource.Animation.anim_slide_in_left,
                Resource.Animation.anim_slide_out_left);
            return base.ShowActivity(view, attribute, request);
        }
    }
}