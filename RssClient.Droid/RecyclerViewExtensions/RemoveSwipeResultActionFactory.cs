﻿using System.Collections.Generic;
using Com.H6ah4i.Android.Widget.Advrecyclerview.Swipeable.Action;
using MvvmCross.AdvancedRecyclerView.Adapters.NonExpandable;
using MvvmCross.AdvancedRecyclerView.Swipe;
using MvvmCross.AdvancedRecyclerView.Swipe.ResultActions;

namespace RssClient.Droid.RecyclerViewExtensions
{
    public class RemoveSwipeResultActionFactory : MvxSwipeResultActionFactory
    {
        public RemoveSwipeResultActionFactory()
        {
            OpenedPositions = new List<int>();
        }


        private List<int> OpenedPositions { get; }

        public override SwipeResultAction GetSwipeLeftResultAction(MvxNonExpandableAdapter adapter, int position)
        {
            if (OpenedPositions.Count > 0)
            {
                foreach (var i in new List<int>(OpenedPositions))
                {
                    OpenedPositions.Remove(i);
                    if (i < adapter.ItemCount)
                    {
                        new MvxSwipeUnpinResultAction(adapter, i).PerformAction();
                    }
                }
            }

            OpenedPositions.Add(position);
            return new MvxSwipeToDirectionResultAction(adapter, SwipeDirection.FromRight, position);
        }

        public override SwipeResultAction GetSwipeRightResultAction(MvxNonExpandableAdapter adapter, int position)
        {
            return new MvxSwipeUnpinResultAction(adapter, position);
        }

        public override SwipeResultAction GetUnpinSwipeResultAction(MvxNonExpandableAdapter adapter, int position)
        {
            OpenedPositions.Remove(position);
            return new MvxSwipeUnpinResultAction(adapter, position);
        }
    }
}