﻿using MvvmCross.AdvancedRecyclerView.Data;
using RssClient.Core.ViewModels;

namespace RssClient.Droid.RecyclerViewExtensions
{
    public class RssMessageItemUniqueIdProvider : IMvxItemUniqueIdProvider
    {
        public long GetUniqueId(object fromObject)
        {
            return (fromObject as RssMessageItemViewModel).UniqueId;
        }
    }
}