﻿using MvvmCross.AdvancedRecyclerView.Data;
using RssClient.Core.ViewModels;

namespace RssClient.Droid.RecyclerViewExtensions
{
    public class RssFeedsItemUniqueIdProvider : IMvxItemUniqueIdProvider
    {
        public long GetUniqueId(object fromObject)
        {
            return (fromObject as RssFeedItemViewModel).UniqueId;
        }
    }
}