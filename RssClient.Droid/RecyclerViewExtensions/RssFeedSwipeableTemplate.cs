﻿using Com.H6ah4i.Android.Widget.Advrecyclerview.Swipeable;
using MvvmCross.AdvancedRecyclerView.TemplateSelectors;

namespace RssClient.Droid.RecyclerViewExtensions
{
    public class RssFeedSwipeableTemplate : IMvxSwipeableTemplate
    {
        public int SwipeContainerViewGroupId => Resource.Id.rss_feed_container;

        public int UnderSwipeContainerViewGroupId => Resource.Id.rss_feed_underSwipe;

        public int SwipeReactionType => SwipeableItemConstants.ReactionCanSwipeBothH;
    }
}