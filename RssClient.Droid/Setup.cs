﻿using MvvmCross;
using MvvmCross.Droid.Support.V7.AppCompat;
using RssClient.Core;
using RssClient.Core.Services.Interfaces;
using RssClient.Droid.Services;

namespace RssClient.Droid
{
    public class Setup : MvxAppCompatSetup<App>
    {
        protected override void InitializeLastChance()
        {
            base.InitializeLastChance();
            Mvx.IoCProvider.RegisterType<IAlertService, AndroidAlertService>();
        }
    }
}