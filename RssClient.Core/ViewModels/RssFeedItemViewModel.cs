﻿using MvvmCross.Commands;
using MvvmCross.ViewModels;
using RssClient.Core.Models;

namespace RssClient.Core.ViewModels
{
    public class RssFeedItemViewModel : MvxViewModel
    {
        private string _name;
        private MvxCommand _removeFeedCommand;
        private string _url;

        public RssFeedItemViewModel(RssFeed feed) : this(feed.Name, feed.Url)
        {
            Id = feed.Id;
            Feed = feed;
        }

        private RssFeedItemViewModel(string name, string url)
        {
            Name = name;
            Url = url;
        }

        public string Id { get; set; }

        public long UniqueId { get; set; }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public MvxCommand RemoveFeedCommand => _removeFeedCommand =
            _removeFeedCommand ?? new MvxCommand(DoRemove);


        public MvxCommand<RssFeedItemViewModel> RemoveItemCommand { get; set; }

        public string Url
        {
            get => _url;
            set => SetProperty(ref _url, value);
        }

        public RssFeed Feed { get; set; }

        private void DoRemove()
        {
            RemoveItemCommand?.Execute(this);
        }

        public void Update()
        {
            Name = Feed.Name;
            Url = Feed.Url;
        }
    }
}