using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AppCenter.Crashes;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using Realms;
using RssClient.Core.Models;
using RssClient.Core.NavigationParams;
using RssClient.Core.Services;
using RssClient.Core.Services.Interfaces;

namespace RssClient.Core.ViewModels
{
    public class RssFeedsViewModel : BaseViewModel
    {
        private MvxCommand _addNewFeedCommand;
        private MvxCommand<RssFeedItemViewModel> _editFeedCommand;
        private MvxObservableCollection<RssFeedItemViewModel> _feeds;
        private MvxCommand<RssFeedItemViewModel> _removeFeedItemCommand;
        private MvxCommand<RssFeedItemViewModel> _selectFeedCommand;
        private IDisposable _token;

        public RssFeedsViewModel()
        {
            RssLoaderService = Mvx.IoCProvider.Resolve<RssLoaderService>();
            DatabaseService = Mvx.IoCProvider.Resolve<DatabaseService>();
            AlertService = Mvx.IoCProvider.Resolve<IAlertService>();
        }

        public IAlertService AlertService { get; }

        public MvxObservableCollection<RssFeedItemViewModel> Feeds
        {
            get => _feeds;
            set => SetProperty(ref _feeds, value);
        }


        public MvxCommand AddNewFeedCommand =>
            _addNewFeedCommand ?? (_addNewFeedCommand = new MvxCommand(DoAddNewRssFeed));

        public MvxCommand<RssFeedItemViewModel> SelectFeedCommand => _selectFeedCommand =
            _selectFeedCommand ?? new MvxCommand<RssFeedItemViewModel>(DoSelectFeed);

        public MvxCommand<RssFeedItemViewModel> RemoveFeedItemCommand => _removeFeedItemCommand =
            _removeFeedItemCommand ?? new MvxCommand<RssFeedItemViewModel>(DoRemoveFeedItem);


        public MvxCommand<RssFeedItemViewModel> EditFeedCommand =>
            _editFeedCommand ?? (_editFeedCommand = new MvxCommand<RssFeedItemViewModel>(
                DoEditFeedAsync));


        public RssLoaderService RssLoaderService { get; }
        public DatabaseService DatabaseService { get; }

        private void DoRemoveFeedItem(RssFeedItemViewModel item)
        {
            DatabaseService.RemoveRssFeed(item.Feed);
        }

        private async void DoEditFeedAsync(RssFeedItemViewModel item)
        {
            var @params = new CreateOrEditRssFeedNavigationParams
            {
                DialogOperationType = DialogOperationType.Edit,
                Feed = item.Feed
            };
            await NavigationService.Navigate<CreateOrEditRssFeedViewModel, CreateOrEditRssFeedNavigationParams>(
                @params);
        }

        private async void DoSelectFeed(RssFeedItemViewModel item)
        {
            var result = Uri.TryCreate(item.Url, UriKind.Absolute, out var uriResult)
                         && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

            if (result)
            {
                await NavigationService.Navigate<RssFeedViewModel, RssFeedItemViewModel>(item);
            }
            else
            {
                AlertService.ShowLongToast("URL rss ����� ������������");
            }
        }

        private async void DoAddNewRssFeed()
        {
            var @params = new CreateOrEditRssFeedNavigationParams
            {
                DialogOperationType = DialogOperationType.Create
            };

            await NavigationService.Navigate<CreateOrEditRssFeedViewModel, CreateOrEditRssFeedNavigationParams>(
                @params);
        }

        public override Task Initialize()
        {
            var items = DatabaseService.RssFeeds.OrderByDescending(feed => feed.CreatedTime).AsRealmCollection();

            Feeds = new MvxObservableCollection<RssFeedItemViewModel>(
                items.Select((feed, i) => new RssFeedItemViewModel(feed)
                    {UniqueId = i, RemoveItemCommand = RemoveFeedItemCommand}));

            _token = items.SubscribeForNotifications(UpdateCallback);

            return base.Initialize();
        }

        private void UpdateCallback(IRealmCollection<RssFeed> sender, ChangeSet changes, Exception error)
        {
            try
            {
                if (changes == null) return;

                if (changes.InsertedIndices?.Length > 0)
                {
                    foreach (var index in changes.InsertedIndices)
                        Feeds.Add(new RssFeedItemViewModel(sender[index])
                            {UniqueId = index, RemoveItemCommand = RemoveFeedItemCommand});
                }

                if (changes.DeletedIndices?.Length > 0)
                {
                    foreach (var index in changes.DeletedIndices.Reverse())
                        Feeds.RemoveAt(index);

                    for (var i = 0; i < Feeds.Count; i++)
                    {
                        var feed = Feeds[i];
                        feed.UniqueId = i;
                    }
                }

                if (changes.ModifiedIndices?.Length > 0)
                {
                    foreach (var index in changes.ModifiedIndices)
                        Feeds[index].Update();
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        public override void ViewDestroy(bool viewFinishing = true)
        {
            _token?.Dispose();
            base.ViewDestroy(viewFinishing);
        }
    }
}