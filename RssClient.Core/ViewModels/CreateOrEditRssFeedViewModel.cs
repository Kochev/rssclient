﻿using System;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using RssClient.Core.Models;
using RssClient.Core.NavigationParams;
using RssClient.Core.Services;

namespace RssClient.Core.ViewModels
{
    public class CreateOrEditRssFeedViewModel : MvxViewModel<CreateOrEditRssFeedNavigationParams>
    {
        private readonly DatabaseService _databaseService;
        private readonly IMvxNavigationService _navigationService;
        private MvxCommand _addNewRssFeedCommand;
        private bool _canSave;
        private MvxCommand _closeCommand;
        private RssFeed _feed;
        private string _name;
        private string _title;
        private string _url;

        public CreateOrEditRssFeedViewModel()
        {
            _navigationService = Mvx.IoCProvider.Resolve<IMvxNavigationService>();
            _databaseService = Mvx.IoCProvider.Resolve<DatabaseService>();
        }

        public DialogOperationType DialogOperationType { get; private set; }

        public string Name
        {
            get => _name;
            set
            {
                SetProperty(ref _name, value);
                UpdateName();
            }
        }


        public MvxInteraction<string> ChangeNameEditError { get; } = new MvxInteraction<string>();
        public MvxInteraction<string> ChangeUrlEditError { get; } = new MvxInteraction<string>();

        public string Url
        {
            get => _url;
            set
            {
                SetProperty(ref _url, value);
                UpdateUrl();
            }
        }

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public bool CanSave
        {
            get => _canSave;
            set => SetProperty(ref _canSave, value);
        }

        public MvxCommand SaveRssFeedCommand => _addNewRssFeedCommand ??
                                                (_addNewRssFeedCommand = new MvxCommand(DoSaveRssFeed));

        public MvxCommand CloseCommand => _closeCommand ?? (_closeCommand = new MvxCommand(DoClose));

        private void UpdateName()
        {
            if (string.IsNullOrEmpty(_name) || string.IsNullOrWhiteSpace(_name))
                ChangeNameEditError.Raise("Имя не может быть пустым!");

            UpdateCanSave();
        }

        private void UpdateCanSave()
        {
            if (string.IsNullOrEmpty(_name) || string.IsNullOrWhiteSpace(_name) || string.IsNullOrEmpty(_url) ||
                string.IsNullOrWhiteSpace(_url))
            {
                CanSave = false;
                return;
            }

            var isUrlValid = Uri.IsWellFormedUriString(_url, UriKind.RelativeOrAbsolute);
            CanSave = isUrlValid;
        }

        private void UpdateUrl()
        {
            if (string.IsNullOrEmpty(_url) || string.IsNullOrWhiteSpace(_url))
            {
                ChangeUrlEditError.Raise("Имя не может быть пустым!");
                UpdateCanSave();
                return;
            }

            //можно проверять более чувствительным регулярным выражением
            var isUrlValid = Uri.IsWellFormedUriString(_url, UriKind.Absolute);
            if (!isUrlValid)
                ChangeUrlEditError.Raise("Url не валиден");

            UpdateCanSave();
        }

        private async void DoClose()
        {
            await _navigationService.Close(this);
        }

        private async void DoSaveRssFeed()
        {
            if (DialogOperationType == DialogOperationType.Edit && _feed != null)
                _databaseService.UpdateRssFeed(_feed, Name, Url);
            else
            {
                _databaseService.AddRssFeed(new RssFeed
                {
                    Name = Name,
                    Url = Url
                });
            }

            await _navigationService.Close(this);
        }

        public override void Prepare(CreateOrEditRssFeedNavigationParams parameter)
        {
            DialogOperationType = parameter.DialogOperationType;
            _feed = parameter.Feed;

            if (DialogOperationType == DialogOperationType.Edit)
            {
                Name = _feed?.Name;
                Url = _feed?.Url;
                Title = "Изменить rss-ленту";
            }
            else
            {
                Title = "Добавить rss-ленту";
                Url = string.Empty;
                Name = string.Empty;
            }
        }
    }
}