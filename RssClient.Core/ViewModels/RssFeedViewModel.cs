﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AppCenter.Crashes;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using Realms;
using RssClient.Core.Models;
using RssClient.Core.Services;
using RssClient.Core.Services.Interfaces;

namespace RssClient.Core.ViewModels
{
    public class RssFeedViewModel : BaseViewModel<RssFeedItemViewModel>
    {
        private bool _isUpdating;
        private RssFeedItemViewModel _itemViewModel;
        private MvxObservableCollection<RssMessageItemViewModel> _messages;
        private string _rssName;
        private MvxCommand<RssMessageItemViewModel> _selectRssMessageCommand;
        private MvxCommand _updateMessagesCommand;

        public RssFeedViewModel()
        {
            RssLoaderService = Mvx.IoCProvider.Resolve<RssLoaderService>();
            DatabaseService = Mvx.IoCProvider.Resolve<DatabaseService>();
            AlertService = Mvx.IoCProvider.Resolve<IAlertService>();

            Messages = new MvxObservableCollection<RssMessageItemViewModel>();
        }

        public MvxInteraction<string> OpenWebLinkInteraction { get; } = new MvxInteraction<string>();

        public IAlertService AlertService { get; set; }

        public MvxObservableCollection<RssMessageItemViewModel> Messages
        {
            get => _messages;
            set => SetProperty(ref _messages, value);
        }

        public RssLoaderService RssLoaderService { get; }
        public DatabaseService DatabaseService { get; }

        public MvxCommand UpdateMessagesCommand => _updateMessagesCommand =
            _updateMessagesCommand ?? new MvxCommand(DoUpdateMessages);

        public MvxCommand<RssMessageItemViewModel> SelectRssMessageCommand => _selectRssMessageCommand =
            _selectRssMessageCommand ?? new MvxCommand<RssMessageItemViewModel>(DoSelectRssMessage);

        public string RssName
        {
            get => _rssName;
            set => SetProperty(ref _rssName, value);
        }

        public bool IsUpdating
        {
            get => _isUpdating;
            set => SetProperty(ref _isUpdating, value);
        }

        private void DoSelectRssMessage(RssMessageItemViewModel message)
        {
            if (!string.IsNullOrEmpty(message.Link) && !string.IsNullOrWhiteSpace(message.Link))
                OpenWebLinkInteraction.Raise(message.Link);
        }

        public override async Task Initialize()
        {
            if (_itemViewModel.Feed.Messages?.Count > 0)
            {
                var id = 0;
                Messages = new MvxObservableCollection<RssMessageItemViewModel>(
                    _itemViewModel.Feed.Messages.Select(message =>
                    {
                        var item = new RssMessageItemViewModel(message) {UniqueId = id};
                        id += 1;
                        return item;
                    }));
            }

            await Task.Run(() => UpdateMessagesCommand.Execute());
        }

        private async void DoUpdateMessages()
        {
            try
            {
                IsUpdating = true;
                var data = await RssLoaderService.GetRssFeedMessages(_itemViewModel.Url);
                DatabaseService.UpdateRssFeedMessages(_itemViewModel.Id, data, UpdateCallback);
            }
            catch (Exception e)
            {
                AlertService.ShowLongToast("Не удалось обновить сообщения");
            }
            finally
            {
                IsUpdating = false;
            }
        }

        private void UpdateCallback(IRealmCollection<RssFeedMessage> sender, ChangeSet changes, Exception error)
        {
            try
            {
                if (changes == null) return;

                if (changes.InsertedIndices?.Length > 0)
                {
                    foreach (var index in changes.InsertedIndices)
                    {
                        Messages.Add(new RssMessageItemViewModel(sender[index])
                            { UniqueId = index });
                    }
                }

                if (changes.DeletedIndices?.Length > 0)
                {
                    foreach (var index in changes.DeletedIndices.Reverse())
                        Messages.RemoveAt(index);

                    for (var i = 0; i < Messages.Count; i++)
                    {
                        var feed = Messages[i];
                        feed.UniqueId = i;
                    }
                }

                if (changes.ModifiedIndices?.Length > 0)
                {
                    foreach (var index in changes.ModifiedIndices)
                        Messages[index].Update();
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        public override void Prepare(RssFeedItemViewModel itemViewModel)
        {
            _itemViewModel = itemViewModel;
            RssName = _itemViewModel.Name;
        }
    }
}