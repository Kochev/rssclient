﻿using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace RssClient.Core.ViewModels
{
    public class BaseViewModel : MvxViewModel
    {
        private MvxCommand _closeCommand;

        public BaseViewModel()
        {
            NavigationService = Mvx.IoCProvider.Resolve<IMvxNavigationService>();
        }

        public MvxCommand CloseCommand => _closeCommand ?? (_closeCommand = new MvxCommand(DoClose));

        public IMvxNavigationService NavigationService { get; }

        private async void DoClose()
        {
            await NavigationService.Close(this);
        }
    }

    public class BaseViewModel<T> : MvxViewModel<T> where T : class
    {
        private MvxCommand _closeCommand;

        public BaseViewModel()
        {
            NavigationService = Mvx.IoCProvider.Resolve<IMvxNavigationService>();
        }

        public MvxCommand CloseCommand => _closeCommand ?? (_closeCommand = new MvxCommand(DoClose));

        public IMvxNavigationService NavigationService { get; }

        private async void DoClose()
        {
            await NavigationService.Close(this);
        }

        public override void Prepare(T parameter)
        {
        }
    }
}