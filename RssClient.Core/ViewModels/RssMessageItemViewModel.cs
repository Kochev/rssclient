﻿using MvvmCross.ViewModels;
using RssClient.Core.Models;

namespace RssClient.Core.ViewModels
{
    public class RssMessageItemViewModel : MvxViewModel
    {
        private readonly RssFeedMessage _message;
        private string _date;
        private string _description;
        private string _title;
        private string _link;

        public RssMessageItemViewModel(RssFeedMessage message)
        {
            _message = message;
            Update();
        }

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        public string Date
        {
            get => _date;
            set => SetProperty(ref _date, value);
        }
        public string Link
        {
            get => _link;
            set => SetProperty(ref _link, value);
        }

        public long UniqueId { get; set; }

        public void Update()
        {
            Title = _message.Title;
            Description = _message.Description;
            Date = _message.Date;
            Link = _message.Link;
        }
    }
}