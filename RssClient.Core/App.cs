using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.ViewModels;
using RssClient.Core.Services;
using RssClient.Core.ViewModels;

namespace RssClient.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            Mvx.IoCProvider.RegisterSingleton(new DatabaseService());
            Mvx.IoCProvider.RegisterSingleton(new RssLoaderService());

            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            RegisterAppStart<RssFeedsViewModel>();
        }
    }
}