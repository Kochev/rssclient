﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.SyndicationFeed;
using MvvmCross;
using MvvmCross.Base;
using Realms;
using RssClient.Core.Models;

namespace RssClient.Core.Services
{
    public class DatabaseService
    {
        private readonly IMvxMainThreadAsyncDispatcher _dispatcher;

        public DatabaseService()
        {
            _dispatcher = Mvx.IoCProvider.Resolve<IMvxMainThreadAsyncDispatcher>();
            Init();
        }

        public IQueryable<RssFeed> RssFeeds => Database.All<RssFeed>();

        public Realm Database { get; set; }

        private async void Init()
        {
            await _dispatcher.ExecuteOnMainThreadAsync(() =>
            {
                Database = Realm.GetInstance();

                //for first app start
                if (!RssFeeds.Any()) Database.Write(FirstStartDatabaseInit);
            });
        }

        public void AddRssFeed(RssFeed feed)
        {
            _dispatcher.ExecuteOnMainThreadAsync(() => { Database.Write(() => { Database.Add(feed, true); }); });
        }


        public void UpdateRssFeed(RssFeed feed, string name, string url)
        {
            _dispatcher.ExecuteOnMainThreadAsync(() =>
            {
                if (feed == null) return;

                using (var transaction = Database.BeginWrite())
                {
                    feed.Name = name;
                    feed.Url = url;
                    transaction.Commit();
                }
            });
        }

        public void UpdateRssFeedMessages(string rssFeedId, List<ISyndicationItem> items,
            NotificationCallbackDelegate<RssFeedMessage> updateCallback)
        {
            _dispatcher.ExecuteOnMainThreadAsync(() =>
            {
                var currentItem = Database.Find<RssFeed>(rssFeedId);

                if (currentItem != null && items?.Count > 0)
                {
                    currentItem.Messages.AsRealmCollection().SubscribeForNotifications(updateCallback);

                    var itemsForUpdate = new List<UpdatedItem>();

                    Database.Write(() =>
                    {
                        foreach (var item in items)
                        {
                            var existedMessage =
                                currentItem.Messages.FirstOrDefault(feedMessage => feedMessage.MessageId == item.Id);
                            if (existedMessage == null)
                            {
                                var url = item.Links?.FirstOrDefault(link =>
                                        link.RelationshipType?.Equals("alternate",
                                            StringComparison.InvariantCultureIgnoreCase) == true)
                                    ?.Uri?.OriginalString;
                                var message = new RssFeedMessage
                                {
                                    MessageId = item.Id,
                                    Title = item.Title,
                                    Description = item.Description,
                                    Date = item.Published.ToString("dd.MM.yyyy"),
                                    Link = url,
                                    CreatedTime = DateTime.Now
                                };
                                currentItem.Messages.Add(message);
                            }
                            else
                                itemsForUpdate.Add(new UpdatedItem(existedMessage.Id, item));
                        }
                    });

                    if (itemsForUpdate.Count > 0)
                    {
                        foreach (var tuple in itemsForUpdate)
                        {
                            var updatedMessage = Database.Find<RssFeedMessage>(tuple.Id);
                            var url = tuple.Message.Links?.FirstOrDefault(link =>
                                    link.RelationshipType?.Equals("alternate",
                                        StringComparison.InvariantCultureIgnoreCase) == true)
                                ?.Uri?.OriginalString;
                            using (var transaction = Database.BeginWrite())
                            {
                                updatedMessage.Title = tuple.Message.Title;
                                updatedMessage.Description = tuple.Message.Description;
                                updatedMessage.Date = tuple.Message.Published.ToString("dd.MM.yyyy");
                                updatedMessage.Link = url;
                                transaction.Commit();
                            }
                        }
                    }
                }
            });
        }

        private void FirstStartDatabaseInit()
        {
            Database.Add(new RssFeed
            {
                Name = "Calend.ru",
                Url = "http://www.calend.ru/img/export/calend.rss",
                CreatedTime = DateTime.Now
            });
            Database.Add(new RssFeed
            {
                Name = "meteoinfo",
                Url = "https://meteoinfo.ru/rss/forecasts/index.php?s=28440",
                CreatedTime = DateTime.Now
            });
            Database.Add(new RssFeed
            {
                Name = "Old-hard",
                Url = "http://www.old-hard.ru/rss",
                CreatedTime = DateTime.Now
            });
        }

        public void RemoveRssFeed(RssFeed feed)
        {
            _dispatcher.ExecuteOnMainThreadAsync(() =>
            {
                if (feed == null) return;

                Database.Write(() => { Database.Remove(feed); });
            });
        }
    }
}