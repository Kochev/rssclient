﻿namespace RssClient.Core.Services.Interfaces
{
    public interface IAlertService
    {
        void ShowLongToast(string message);
    }
}