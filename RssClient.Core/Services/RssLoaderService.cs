﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.SyndicationFeed;
using Microsoft.SyndicationFeed.Rss;

namespace RssClient.Core.Services
{
    public class RssLoaderService
    {
        public async Task<List<ISyndicationItem>> GetRssFeedMessages(string url)
        {
            var uri = new UriBuilder(url).Uri.ToString();

            var data = new List<ISyndicationItem>();

            using (var xmlReader = XmlReader.Create(uri))
            {
                var feedReader = new RssFeedReader(xmlReader);

                // Read the feed
                while (await feedReader.Read())
                    switch (feedReader.ElementType)
                    {
                        // Read Item
                        case SyndicationElementType.Item:
                            var item = await feedReader.ReadItem();
                            if (item != null) data.Add(item);

                            break;
                    }
            }

            return data;
        }
    }
}