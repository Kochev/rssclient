﻿namespace RssClient.Core.Models
{
    public enum DialogOperationType
    {
        Create = 1,
        Edit = 2
    }
}