﻿using System;
using Microsoft.SyndicationFeed;

namespace RssClient.Core.Models
{
    public class UpdatedItem : Tuple<string, ISyndicationItem>
    {
        public UpdatedItem(string name, ISyndicationItem message) : base(name, message)
        {
        }

        public string Id => Item1;
        public ISyndicationItem Message => Item2;
    }
}