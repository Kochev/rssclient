﻿using System;
using Realms;

namespace RssClient.Core.Models
{
    public class RssFeedMessage : RealmObject
    {
        [PrimaryKey]
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string MessageId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Date { get; set; }
        public string Link { get; set; }
        public DateTimeOffset CreatedTime { get; set; }
    }
}