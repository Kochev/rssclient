﻿using System;
using System.Collections.Generic;
using Realms;

namespace RssClient.Core.Models
{
    public class RssFeed : RealmObject
    {
        [PrimaryKey] public string Id { get; set; } = Guid.NewGuid().ToString();

        public string Name { get; set; }
        public string Url { get; set; }
        public DateTimeOffset CreatedTime { get; set; }
        public IList<RssFeedMessage> Messages { get; }
    }
}