﻿using RssClient.Core.Models;

namespace RssClient.Core.NavigationParams
{
    public class CreateOrEditRssFeedNavigationParams
    {
        public DialogOperationType DialogOperationType { get; set; }
        public RssFeed Feed { get; set; }
    }
}